# tcpdump-builder

This script is designed to help build a tcpdump command based on what the user would like to capture.

---------------------

## Items to capture
- Interface
  - Multiple, all, single
- Timed, Rolling, Scheduled
  - Timed
    - Segments and duration
  - Rolling
    - Time vs Size
    - Max before roll over
  - Scheduled
    - Segments and duration
    - Time to start the capture
- Protocol
  - Port
- Snap length suggestion based on Protocol
- Nodes
  - Single, all, selection
- Host filter
  - Single, Multiple, all
