# This script will assist in building a tcpdump command.
version = 0.1
lastModified = "2020-06-28"

# Gather number of nodes.
print ('This script will build a tcpdump command based on what you need to capture.')
print ('    Version: {0}, Last Modifed: {1}\n'.format(version, lastModified))

interface = input ('How many interfaces do you want to capture? (A)ll, (M)ultiple, (S)ingle : ')
if interface == "A":
    print ("Capturing All interfaces\n")
elif interface == "M":
    print ("Capturing Multiple interfaces\n")
